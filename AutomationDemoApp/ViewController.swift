//
//  ViewController.swift
//  AutomationDemoApp
//
//  Created by Divya Suthar on 11/03/19.
//  Copyright © 2019 informationworks. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet var textField: UITextField!
  @IBOutlet var label: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  @IBAction func submit(_ sender: Any) {
    label.text = textField.text
  }
  
}

